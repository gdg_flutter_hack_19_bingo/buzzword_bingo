class BingoPlayer {
  String id;
  String name;
  List<String> words;

  BingoPlayer({this.id, this.name, this.words});

  factory BingoPlayer.fromMap(Map map) => new BingoPlayer(
      id: map["key"],
      name: map["name"],
      words: map["words"] == null ? [] : map["words"].cast<String>()
  );

  Map toMap() => {
    "key": id,
    "name": name,
    "words": words
  };
}
