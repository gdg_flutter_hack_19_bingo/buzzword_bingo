import 'dart:io';

import 'package:buzzword_bingo/models/session.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

import 'package:buzzword_bingo/models/sessionDB.dart';

class DBProvider {
  DBProvider._();
  static final DBProvider db = DBProvider._();

  static Database _database;
  static const String TABLE_NAME = 'Bingosessions';

  Future<Database> get database async {
    if (_database != null)
      return _database;

    // if _database is null we instantiate it
    _database = await initDB();
    return _database;
  }

  initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    // alternatively using the default databasepath
    /*var databasePath = await getDatabasesPath();
    String path = join(databasePath, "AnimeApp.db");
    // Make sure the directory exists
    try {
      await Directory(databasePath).create(recursive: true);
    } catch (_) {}*/
    String path = join(documentsDirectory.path, "bingosessions.db");
    return await openDatabase(path, version: 1, onOpen: (db) {
    }, onCreate: (Database db, int version) async {
      await db.execute("CREATE TABLE $TABLE_NAME ("
          "sessionkey TEXT PRIMARY KEY,"
          "sessionname TEXT NOT NULL,"
          "username TEXT NOT NULL,"
          "active NUMERIC"
          ")");
    });

    /* ----------------------- ADDITIONAL INFO ABOUT SQLITE PRIMARY KEY -----------------------
    You get one for free, called ROWID. This is in every SQLite table whether you ask for it or not.

    If you include a column of type INTEGER PRIMARY KEY (!!!!!!!!!!!!!!IMPORTANT IT IS INTEGER PRIMARY KEY!!!!!!!!), that column points at (is an alias for) the automatic ROWID column.
    ROWID (by whatever name you call it) is assigned a value whenever you INSERT a row, as you would expect. If you explicitly assign a non-NULL value on INSERT, it will get that specified value instead of the auto-increment.
    If you explicitly assign a value of NULL on INSERT, it will get the next auto-increment value.*/
  }


  // ============================================ Favorite Queries ============================================

  // ---------- CREATE ----------
  newSession(Session newSession) async {

    print('TEST: ' + newSession.sessionname);

    final db = await database;
    // rawInsert Methode:
    var res = await db.rawInsert(
        "INSERT INTO $TABLE_NAME (sessionkey,sessionname,username,active)"
            " VALUES ('${newSession.sessionkey}','${newSession.sessionname}','${newSession.username}','${newSession.active}')");
    // Insert Methode:
    //var res = await db.insert("Favorite", newFavorite.toJson());
    //print('SHOULD BE ADDED!!');
    return res;
  }

  updateSession(Session session) async{
    final db = await database;
    var res = await db.update(TABLE_NAME, session.toJson(), where: "sessionkey = ?", whereArgs: [session.sessionkey]);
    return res;
  }

  Future<List<Session>> getAllSessions() async{
    final db = await database;
    var res = await db.query(TABLE_NAME);
    List<Session> list = res.isNotEmpty ? res.map((c) => Session.fromJson(c)).toList() : [];
    //print(list);
    return list;
  }

  Future<List<Session>> getActiveSessions() async{
    final db = await database;
    var res = await db.query(TABLE_NAME, where: "active = 1");
    List<Session> list = res.isNotEmpty ? res.map((c) => Session.fromJson(c)).toList() : [];
    //print(list);
    return list;
  }

  Future<List<Session>> getInactiveSessions() async{
    final db = await database;
    var res = await db.query(TABLE_NAME, where: "active = 0");
    List<Session> list = res.isNotEmpty ? res.map((c) => Session.fromJson(c)).toList() : [];
    //print(list);
    return list;
  }

  clearDeletedSessions(List<String> sessionsKeys)async{
    final db = await database;
    (await getActiveSessions()).where((s)=>!sessionsKeys.contains(s.sessionkey)).forEach((s){
      db.rawDelete("DELETE FROM $TABLE_NAME WHERE sessionkey='${s.sessionkey}'");
    });
  }

  clearSessions() async{
    final db = await database;
    db.rawDelete("DELETE FROM $TABLE_NAME");
    print('SAVED SESSIONS SHOULD BE CLEARED');
  }
}
