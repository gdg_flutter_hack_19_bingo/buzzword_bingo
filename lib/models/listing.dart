import 'package:firebase_database/firebase_database.dart';

class BingoListing {
  String id;
  String name;

  BingoListing({this.id, this.name});

  factory BingoListing.fromMap(Map map) => new BingoListing(
    id: map["id"],
    name: map["name"]
  );

  Map toMap() => {
    "id": id,
    "name": name
  };

  void delete() {
    final ref = FirebaseDatabase.instance.reference();
    ref.child('session-keys').child(id).remove();
    ref.child('sessions').child(id).remove();
  }
}
