import 'dart:convert';

Session favoriteFromJson(String str) {
  final jsonData = json.decode(str);
  return Session.fromJson(jsonData);
}

String favoriteToJson(Session data) {
  final dyn = data.toJson();
  return json.encode(dyn);
}

class Session {
  String sessionkey;
  String sessionname;
  String username;
  int active;       // 1 = true, 0 = false

  Session({
    this.sessionkey,
    this.sessionname,
    this.username,
    this.active,
  });

  factory Session.fromJson(Map<String, dynamic> json) => new Session(
    sessionkey: json["sessionkey"],
    sessionname: json["sessionname"],
    username: json["username"],
    active: json["active"],
  );

  Map<String, dynamic> toJson() => {
    "sessionkey": sessionkey,
    "sessionname": sessionname,
    "username": username,
    "active": active,
  };
}