import 'package:buzzword_bingo/models/listing.dart';
import 'package:buzzword_bingo/models/player.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:tuple/tuple.dart';

class BingoSession {
  DateTime startTime;
  DateTime endTime;

  String id;
  String name;

  int gridSize;
  List<String> acceptedWords = [];

  List<BingoPlayer> players = [];

  // TODO Location Stuff
  // Location and radius

  BingoSession({this.id, this.name, this.gridSize,this.players,this.acceptedWords});

  factory BingoSession.fromMap(String key, Map map) => new BingoSession(
      id: key,
      name: map["name"],
      gridSize: map["size"],
      acceptedWords: map["words"] == null ? [] : map["words"].cast<String>(),
      players: map['players'].values.map((p)=>BingoPlayer.fromMap(p)).toList().cast<BingoPlayer>()
  );

  Map toMap() => {
    "key": id,
    "name": name,
    "words": acceptedWords,
    "players": players.map((p)=>p.toMap())
  };

  List<BingoPlayer> finishedPlayers() {
    return players
        .where((p) => p.words.every((x) => acceptedWords.contains(x)));
  }

  List<Tuple2<BingoPlayer, num>> getPlayerPoints() {
    var t = players
        .map((p) => new Tuple2(
            p, p.words.where((w) => acceptedWords.contains(w)).length))
        .toList();
    t.sort((x,y)=>y.item2-x.item2);
    return t;
  }

  Future addAccepted(String word) async {
    final ref = FirebaseDatabase.instance
        .reference()
        .child("sessions")
        .child(id)
        .child("words");
    var list = (await ref.once()).value as List;
    if (list == null) list = [];
    list = List.from(list);
    list.add(word);
    ref.set(list);
  }

  static Future<BingoSession> getBingoSessionOnce(String key) async {
    final ref = FirebaseDatabase.instance
        .reference()
        .child("sessions")
        .child(key)
        .once();

    return ref.then((s) {
      final val = s.value;
      return BingoSession.fromMap(key, val);
    });
  }

  static Stream<BingoSession> getBingoSession(String key) {
    final ref =
        FirebaseDatabase.instance.reference().child("sessions").child(key);
    return ref.onValue.map((x) {
      final val = x.snapshot.value;
      return BingoSession.fromMap(key, val);
    });
  }

  static Future<BingoPlayer> joinPlayer(
      String key, String name, List<String> words) async {
    final player = FirebaseDatabase.instance
        .reference()
        .child("sessions")
        .child(key)
        .child("players")
        .push();
    player.child("name").set(name);
    player.child("words").set(words);
    player.child("key").set(player.key);
    return BingoPlayer(id: player.key, name: name, words: words);
  }

  static BingoListing createBingoSession(String name, int size) {
    final ref = FirebaseDatabase.instance.reference();
    final db = ref.child("sessions").push();
    db.child("name").set(name);
    db.child("size").set(size);
    db.child("words").set([]);
    ref.child("session-keys").child(db.key).child("id").set(db.key);
    ref.child("session-keys").child(db.key).child("name").set(name);
    return BingoListing(id: db.key, name: name);
  }

  static Future<List<BingoListing>> getBingoSessions() async {
    final db = await FirebaseDatabase.instance
        .reference()
        .child("session-keys")
        .once();
    if(db.value==null)return null;
    return getBingoListingsFromMap(db.value as Map);
  }

  static Future<BingoListing> getBingoListing(String id) async {
    final db = await FirebaseDatabase.instance
        .reference()
        .child("session-keys").child(id)
        .once();
    if(db.value==null) return null;
    return BingoListing.fromMap(db.value as Map); //TODO optimize
  }

  static List<BingoListing> getBingoListingsFromMap(Map map) {
    final sessions =
        map.values.toList().map((x) => BingoListing.fromMap(x));
    return sessions.toList();
  }

  removeAccepted(String name) async {
    final ref = FirebaseDatabase.instance
        .reference()
        .child("sessions")
        .child(id)
        .child("words");
    var list = (await ref.once()).value as List;
    if (list == null) list = [];
    list = List.from(list);
    list.remove(name);
    ref.set(list);
  }



}
