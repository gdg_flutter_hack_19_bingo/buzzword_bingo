import 'package:buzzword_bingo/models/user.dart';

class BingoGame {
  DateTime startTime;
  DateTime endTime;
  
  int gridSize;
  List<String> acceptedWords;

  List<BingoUser> users;
}
