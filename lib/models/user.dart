import 'package:buzzword_bingo/models/game.dart';

class BingoUser {
  String id;
  String names;

  int totalScore;

  List<BingoGame> history;
}
