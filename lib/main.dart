import 'package:buzzword_bingo/models/Database.dart';
import 'package:buzzword_bingo/models/sessionDB.dart';
import 'package:buzzword_bingo/widgets/bingo_game_screen_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'models/listing.dart';
import 'models/player.dart';
import 'models/session.dart';
import 'widgets/listings.dart';
import 'widgets/session_create.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Buzzword Bingo',
      theme: ThemeData(
        primarySwatch: Colors.amber,
      ),
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  void initState() {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    super.initState();
  }

  Widget _buildActiveSessions() {
    BingoSession.getBingoSessions().then((z) {
      if (z != null)
        DBProvider.db.clearDeletedSessions(z.map((x) => x.id).toList());
    });

    return FutureBuilder<List<Session>>(
      future: DBProvider.db.getActiveSessions(),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          if (snapshot.data.length == 0) {
            return Center(
              child: Text('No Active Sessions'),
            );
          }
          return ListView.builder(
            itemCount: snapshot.data.length,
            itemBuilder: (_, i) {
              Session session = snapshot.data[i];
              return MaterialButton(
                  padding: EdgeInsets.all(0),
                  onPressed: () async {
                    BingoListing l =
                        await BingoSession.getBingoListing(session.sessionkey);
                    BingoPlayer p = (await BingoSession.getBingoSessionOnce(
                            session.sessionkey))
                        .players
                        .firstWhere((x) => x.name == session.username);
                    return Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => BingoGameScreenPage(l, p)),
                    );
                  },
                  child: ListTile(
                    title: Text(session.sessionname),
                    subtitle: Text(session.sessionkey),
                  ));
            },
          );
        } else {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
      },
    );
  }

  Widget _buildHistory() {
    return FutureBuilder<List<Session>>(
      future: DBProvider.db.getInactiveSessions(),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          if (snapshot.data.length == 0) {
            return Center(
              child: Text('No Inactive Sessions'),
            );
          }
          return ListView.builder(
            itemCount: snapshot.data.length,
            itemBuilder: (_, i) {
              Session session = snapshot.data[i];
              return SizedBox(
                  child: ListTile(
                title: Text(session.sessionname),
                subtitle: Text(session.sessionkey),
              ));
            },
          );
        } else {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          bottom: TabBar(
            tabs: [
              Tab(
                child: Text('Active Sessions'),
              ),
              Tab(
                child: Text('Open Sessions'),
              ),
              Tab(
                child: Text('History'),
              ),
            ],
          ),
          title: Text('Bullshit-Bingo'),
        ),
        body: SafeArea(
          child: TabBarView(
            children: [
              _buildActiveSessions(),
              BingoListings(),
              _buildHistory(),
            ],
          ),
        ),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add),
          onPressed: () => Navigator.of(context).push(
              new MaterialPageRoute(builder: (context) => SessionCreate())),
        ),
      ),
    );
  }
}
