import 'package:buzzword_bingo/models/player.dart';
import 'package:buzzword_bingo/models/session.dart';
import 'package:flutter/material.dart';

import 'bingo_cell.dart';

class BingoGrid extends StatelessWidget {
  BingoSession session;
  BingoPlayer player;
  bool isLocalPlayer;

  BingoGrid(this.player, this.session, this.isLocalPlayer);

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      Center(
        child: Column(
          children: <Widget>[
            Text(
              "User",
              style: TextStyle(fontSize: 40),
            ),
            Container(
                child: Text(
              player.name,
              style: TextStyle(
                  color: this.isLocalPlayer ? Colors.green : Colors.black,
                  fontSize: 30,
                  fontWeight: FontWeight.w900),
            )),
          ],
        ),
      ),
      Divider(),
      Expanded(
        flex: 3,
        child: GridView.count(
            crossAxisCount: session.gridSize,
            children:
                player.words.map((s) => new BingoCell(s, session)).toList()),
      ),
      Text('Points',style: TextStyle(fontSize: 20),),
      Divider(),
      Expanded(
        flex: 1,
        child: ListView(
          children: session.getPlayerPoints().map((x)=>ListTile(
            title: Text(x.item1.name),
            trailing: Text(x.item2.toString()),
          ) ).toList(),),
      )
    ]);
  }
}
