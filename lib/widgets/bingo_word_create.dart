import 'package:buzzword_bingo/models/Database.dart';
import 'package:buzzword_bingo/models/listing.dart';
import 'package:buzzword_bingo/models/session.dart';
import 'package:buzzword_bingo/models/sessionDB.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'bingo_game_screen_page.dart';

class _BingoWordCreateState extends State<BingoWordCreate> {
  List<String> words = new List();
  String name;
  String error='';
  final TextEditingController eCtrl = new TextEditingController();
  final _focusNode = FocusNode();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Join Game: ${widget.listing.name}"),
          actions: <Widget>[
            IconButton(
                icon: Icon(
                  Icons.arrow_forward,
                  color: widget.size == words.length && name != null
                      ? Colors.white
                      : Colors.grey,
                ),
                onPressed: widget.size == words.length && name != null
                    ? _onBingoStart
                    : null)
          ],
        ),
        body: Card(
          child: Padding(
            padding: EdgeInsets.all(10),
            child: Column(
              children: <Widget>[
                TextField(
                  decoration: InputDecoration(labelText: 'Username'),
                  autofocus: true,
                  onChanged: (text) {
                    name = text;
                    setState(() {});
                  },
                  onSubmitted: (text) {
                    FocusScope.of(context).requestFocus(_focusNode);
                  },
                ),
                TextField(
                    controller: eCtrl,
                    decoration: InputDecoration(labelText: 'Bingo word'),
                    focusNode: _focusNode,
                    onSubmitted: (text) {
                      if (words.contains(text)) {
                        error = "Word already used!";
                      } else if (widget.size == words.length) {
                        error = "Words are full";
                      } else if (text == null || text.length == 0) {
                        error = "Empty words are not allowed";
                      } else {
                        error = '';
                        words.add(text);
                        eCtrl.clear();
                      }
                      FocusScope.of(context).requestFocus(_focusNode);
                      setState(() {});
                    }),
                Divider(),
                Expanded(
                  child: Wrap(
                    alignment: WrapAlignment.center,
                    spacing: 10,
                    children: words.map((w)=>Chip(
                        label: new Text(w),
                        onDeleted: () {
                          words.remove(w);
                          setState(() {});
                        })).toList(),
                  )
                ),
                Divider(),
                Text("${widget.size - words.length} words left to add!"),
                Text("${error}"),
              ],
            ),
          ),
        ));
  }

  _onBingoStart() async {
    BingoListing bingoListing = widget.listing;
    words.shuffle();
    var player = await BingoSession.joinPlayer(bingoListing.id, name, words);

    Session newSession = new Session(
        active: 1,
        sessionname: bingoListing.name,
        sessionkey: bingoListing.id,
        username: name);
    DBProvider.db.newSession(newSession);

    Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => BingoGameScreenPage(bingoListing, player)),
    );
  }
}

class BingoWordCreate extends StatefulWidget {
  final num size;

  final BingoListing listing;

  BingoWordCreate(this.listing, this.size);

  @override
  _BingoWordCreateState createState() => _BingoWordCreateState();
}
