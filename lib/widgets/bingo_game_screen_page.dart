import 'package:buzzword_bingo/main.dart';
import 'package:buzzword_bingo/models/listing.dart';
import 'package:buzzword_bingo/models/player.dart';
import 'package:buzzword_bingo/models/session.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'bingo_grid.dart';

class BingoGameScreenPage extends StatelessWidget {
  BingoListing bingoListing;
  BingoPlayer localplayer;

  BingoGameScreenPage(this.bingoListing, this.localplayer);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Bingo Game: " + bingoListing.name),
      ),
      body: StreamBuilder<BingoSession>(
          stream: BingoSession.getBingoSession(bingoListing.id),
          builder: (context, asyncSnapshot) {
           // if (asyncSnapshot.hasError) throw asyncSnapshot.error;
            if (!asyncSnapshot.hasData)
              return Center(child: CircularProgressIndicator());
            BingoSession s = asyncSnapshot.data;
            return PageView(
              children: s.players
                  .map((p) => BingoGrid(p, s, p.id == localplayer.id))
                  .toList(),
            );
          }),
      floatingActionButton: FloatingActionButton(
        onPressed: (){_stopgame(context);},
        child: Icon(Icons.delete_forever),
      ),
    );
  }

  void _stopgame(BuildContext context) {
    showDialog(context: context,
      child: AlertDialog(
        title: Text('Do you really want to delete this session'),
        actions: <Widget>[
          FlatButton(
            child: Text('Abort'),
            onPressed: (){Navigator.of(context).pop();},
          ),
          FlatButton(
            child: Text('Delete'),
            onPressed: (){ bingoListing.delete();
            Navigator.of(context).pop();
           },
          )
        ],
      )
    );

  }
}
