import 'package:buzzword_bingo/models/session.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';

import 'bingo_word_create.dart';

class BingoListings extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: FirebaseDatabase.instance.reference().child("session-keys").onValue,
      builder: (context, AsyncSnapshot<Event> snapshot) {
        if(!snapshot.hasData)
          return CircularProgressIndicator();
        if(snapshot.data.snapshot.value==null){
          return Padding(
            padding: const EdgeInsets.all(8.0),
            child: Center(child: Text('No Sessions available')),
          );
        }
        final listings = BingoSession.getBingoListingsFromMap(snapshot.data.snapshot.value as Map);
        return ListView.builder(
          itemCount: listings.length,
          itemBuilder: (_, i) {
            final listing = listings[i];
            return MaterialButton(
                padding: EdgeInsets.all(0),
                onPressed: () =>  Navigator.of(context).push(new MaterialPageRoute(builder: (context) => BingoWordCreate(listing,9))),
                child: ListTile(
              title: Text(listing.name),
              subtitle: Text(listing.id),
            ));
          },
        );
      },
    );
  }
}
