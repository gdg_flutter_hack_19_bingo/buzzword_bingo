import 'package:buzzword_bingo/models/listing.dart';
import 'package:flutter/material.dart';
import 'package:buzzword_bingo/widgets/bingo_word_create.dart';
import 'package:buzzword_bingo/models/session.dart';

class SessionCreate extends StatefulWidget{
  @override
  State<SessionCreate> createState() => _SessionCreateState();

}

class _SessionCreateState extends State<SessionCreate>{

  final TextEditingController _controller = new TextEditingController();
  final _focusNode = FocusNode();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Create new Session"),
      ),
      body: Padding(
        padding: EdgeInsets.all(10),
        child: Column(
          children: <Widget>[
            TextField(
              controller: _controller,
              autofocus: true,
              focusNode: _focusNode,
              onSubmitted: (text) {
                if(_controller.text.isEmpty){
                  showDialog(
                    context: context,
                    builder: (context){
                      return AlertDialog(
                        title: Text('Missing attribute'),
                        content: Text('Please enter a Session name'),
                        actions: <Widget>[
                          new FlatButton(
                              onPressed: () => Navigator.of(context).pop(),
                              child: Text('Ok')
                          )
                        ],
                      );
                    }
                  );
                } else{
                  BingoListing l =  BingoSession.createBingoSession(_controller.text, 3);
                  Navigator.of(context).pushReplacement(new MaterialPageRoute(builder: (context) => BingoWordCreate(l,9)));
                }
              }
            )
          ],
        ),
      ),
    );
  }

}
