import 'package:buzzword_bingo/models/session.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class BingoCell extends StatelessWidget {
  String name;
  BingoSession session;
  bool occurred;

  BingoCell(this.name,this.session){
    this.occurred = session.acceptedWords.contains(name);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      padding: EdgeInsets.all(0),
      onPressed: ()=> _cellPressed(context,name),
        child: Padding(
            padding: EdgeInsets.all(4),
            child: ClipRRect(
                borderRadius: BorderRadius.circular(10.0),
                child: Container(
                    decoration: BoxDecoration(
                        color: occurred ? Colors.green : Colors.grey),
                    child: Center(child: Text(name))))));
  }

  _cellPressed(BuildContext context,String name) {
    if(!occurred){
      occurred = true;
      final snackBar = SnackBar(
          content: Text('Added ' + this.name + ' to bingo list!'),
          action:SnackBarAction(label: "Undo", onPressed: () => session.removeAccepted(name)),
      );
      Scaffold.of(context).showSnackBar(snackBar);
      session.addAccepted(name);
      //TODO
    }else{
      occurred = false;
      final snackBar = SnackBar(
        content: Text('Removed ' + this.name + ' from bingo list!'),
        action:SnackBarAction(label: "Undo", onPressed: () => session.addAccepted(name)),
      );
      Scaffold.of(context).showSnackBar(snackBar);
      session.removeAccepted(name);
    }
  }
}
