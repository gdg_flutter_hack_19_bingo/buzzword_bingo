# buzzword_bingo

[![Codemagic build status](https://api.codemagic.io/apps/5cf242d5f4cf640015532538/5cf242d5f4cf640015532537/status_badge.svg)](https://codemagic.io/apps/5cf242d5f4cf640015532538/5cf242d5f4cf640015532537/latest_build)

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our 
[online documentation](https://flutter.dev/docs), which offers tutorials, 
samples, guidance on mobile development, and a full API reference.
